---
title: "Success Services Management"
Description: "The Success Services Management team ensures our paid success offering is impactful for our customers and that the field teams are supported in selling and delivering it."
---

## Success Services Management

The Success Services Management team combines technical expertise with a customer-centric approach to shape, develop, and deliver innovative paid success plan offerings and service designs. This team is responsible for ensuring the effectiveness and impact of GitLab's paid success offerings while supporting field teams in selling and delivering these services. The team focuses on meeting success plan services revenue targets and contributing to the company's growth ARR and renewal targets.

## Levels

### Success Services Manager (Intermediate)

The Success Services Manager (Intermediate) reports to the Director, Success Services

#### Success Services Manager (Intermediate) Job Grade

The SSM (Intermediate) is a job grade [level 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Success Services Manager Responsibilities

- Work closely with customer success and support teams to ensure effective service delivery and customer satisfaction.
- Gather feedback from customers and the sales team to identify areas for improvement in the success services offering and customer experience
- Collaborate with the sales team to identify and pursue opportunities to sell success services to new and existing customers
- Drive the success and adoption of our paid success plan offerings and services within a designated region (EMEA, AMER or APJ)
- Sales Enablement: Collaborate with sales teams to develop and deliver compelling service offerings, value propositions, and commercial collateral. Explain GitLab Services value proposition, catalog of service offerings, and methodologies.
- Participate in sales meetings, product demos, and customer calls to provide expertise on success services and how they can help customers achieve their goals.
- Performance Tracking: Monitor and report on key performance indicators for service offerings, identifying trends and areas for improvement.
- Continuous Improvement: Drive continuous improvement of service offerings based on customer feedback, performance data, and best practices.
- Stakeholder Management: Build and maintain strong relationships with key stakeholders, including customers, partners, and internal teams.

### Success Services Manager Requirements

- Proven experience in customer success services or professional services, preferably in DevOps or a similar technical domain.
- Strong understanding of the DevOps methodology, software development lifecycle, and related technologies
- Ability to develop and maintain strong relationships with customers, understanding their needs, and identifying opportunities to improve their experience and time to value
- Excellent communication and interpersonal skills, with the ability to collaborate effectively with sales teams, customers, and internal stakeholders.
- Data-driven mindset with the ability to analyze customer data, identify trends, and make data-informed decisions to optimize success services and improve customer time to value.
- Experience with customer success metrics, KPIs, and reporting.
- Strong problem-solving skills and the ability to think creatively to develop innovative solutions that address customer challenges and improve their experience.
- Experience in creating and delivering compelling presentations, demonstrations, and training sessions to showcase the value of success services and educate customers on best practices.
- Collaborative and team-oriented mindset, with the ability to work closely with cross-functional teams, including product management, engineering, and marketing, to align success services with company goals and objectives.
- Willingness to travel as required - up to 25%.

### Senior Success Services Manager

The Senior Success Services Manager reports to the Director, Success Services

#### Senior Success Services Manager Job Grade

The Senior Success Services Manager is a job grade [level 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Senior Success Services Manager Responsibilities

[content to be added when the need for the role arises]

### Senior Success Services Manager Requirements

[content to be added when the need for the role arises]

### Director, Success Services

The Director, Success Services is a strategic leader, blending technical acumen with a customer-centric approach. They are accountable for shaping, developing, and delivering our innovative paid success plan offerings and service designs, focusing on exceeding the evolving needs of our customers. The director role involves coordinating with various internal and external stakeholders to set a long-term vision and strategy for our Service offerings, ensuring they align with company goals and drive customer value and business growth.

The Director, Success Services reports to the VP, Customer Success

#### Director, Success Services Job Grade

The Director, Success Services is a job grade [level 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Director, Success Services Responsibilities

- Strategic Leadership: Develop and articulate a clear vision for paid success plan offerings, ensuring alignment with company goals and customer needs.
- Product and Service Development: Lead the design and iterative development of new and existing service offerings, ensuring they are customer-centric, scalable, and technologically advanced.
- Cross-functional Collaboration: Work closely with teams across sales, marketing, product development, customer success, and support to ensure effective service delivery and alignment.
- Market Research: Conduct thorough analysis to understand customer needs, industry trends, competitor offerings, and technology advancements.
- Customer Experience: Develop detailed customer journey maps and manage customer feedback loops to enhance service offerings and customer touchpoints.
- Performance Management: Define and monitor KPIs for service offerings, evaluating success and identifying improvement areas.
- Team Leadership: Guide and support a diverse team, fostering a culture of innovation and continuous improvement.
- Communication: Regularly report on service offerings' progress and outcomes to senior leadership and stakeholders.
- Commercial Management: Oversee the services product portfolio, manage commercial collateral, and ensure alignment with commercial targets.

### Director, Success Services Requirements

- Proven track record of building and maintaining strong relationships with sales teams and customers to drive revenue growth and customer satisfaction.
- Deep understanding of the DevOps landscape, software development lifecycle, and the challenges faced by organizations adopting DevOps practices.
- Excellent communication and interpersonal skills, with the ability to effectively collaborate with cross-functional teams, including sales, product management, engineering, and marketing.
- Strong leadership and people management skills, with experience in building, mentoring, and managing high-performing teams.
- Data-driven and analytical mindset, with the ability to leverage customer insights, usage metrics, and feedback to identify opportunities for improving services and reducing time to value.
- Skilled in creating and delivering compelling presentations and demonstrations to showcase the value of success services to potential customers.
- Ability to think strategically and execute tactically, balancing short-term wins with long-term objectives.
- Strong project management skills, with the ability to prioritize and manage multiple initiatives simultaneously.
- Experience in defining and tracking key performance indicators (KPIs) related to customer adoption

## Performance Indicators

- Pipeline and Revenue created
- Customer Satisfaction
- Renewal of Service
- [Customer adoption metrics](/handbook/customer-success/csm/)

## Career Ladder

Team members in the Success Service Management job family may progress through the career ladder; in some instances team members may move to other roles in the Sales organization such as Professional Services, or other Customer Success roles.

## Hiring Process (All Roles)

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

1. Phone screen with a GitLab Recruiting Team Member
1. Video Interview with the Hiring Manager
1. Team Interviews with 1-4 Team Members

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
