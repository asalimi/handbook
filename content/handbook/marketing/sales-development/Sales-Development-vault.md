---

title: Sales Development Knowledge Vault
description: Information and knowledge that can help to improve your skills. Includes managerial-level processes, manager or team member onboarding, and tool walkthroughs.

---

## Sales Development Knowledge Vault

This page is part of the Sales Development team's handbook [page](/handbook/marketing/sales-development/) and aims to be the in-depth knowledge base of our org. The content below is sourced from the input of many past team members and is here for you to perfect your inbound and outbound sales development skills.

Please review at your own leasure, and we're excited to receive your contributions as well!

## Best Practices for Outbound BDRs

### Cold-Calling and Email Checklist

Our cold-calling best practices typically consist of 4 elements. A pattern interrupt, an elevator pitch and, if required, objection/trap-setting questions and Up-Front Contracts.

To be effective, these need to be customized to the individuals we call as per the logic below:

- Different Geographical Business Cultures
- Personality Type as per the DISC model
- Individual’s Role and Responsibilities
- Business’ Needs and Objectives

### Call Scripts

You can find three examples of our cold call scripts in the description of the [BDR 101 Level Up Course](https://university.gitlab.com/learn/course/bdr101-outbound-cold-calling/bdr101-outbound-calls/f50b8a1c-712b-44e7-a30f-2ddbbc835ccf?client=internal-team-members&page=5)

The main documentation may be [found here](https://docs.google.com/document/d/1D3iV_WW5fRidRN5H8-3SZVAAr3ffEvjxUC6cW5SFXDM/edit) and the library of resources [here](https://drive.google.com/drive/folders/1VhxVwy4DoEvFco_wXeOLb0DLtVIfo7wQ).

### Up-Front Contract

Typically used for scheduled calls and Up-Front Contracts. UFCs can also be used as a defense mechanism when cold calling does not go as planned. For cold-calling, UFCs are used when the prospect can commit to 15’ at the time of the call. If they commit to a 15’ for a later time, UFCs are used to open the call and set the agenda then.

Explore the main documentation [here](https://docs.google.com/document/d/1Y7qEq8g3eHh5-oagERGvNmatiOV3JXi9Tw46SKWwpNM/edit).

### Decision Maker Discovery

The questions below are suggestions to be used while doing a discovery call, in order to identify the decision making process, and to meet the outbound accepted SAO as per the guidelines [here.](/handbook/sales/field-operations/gtm-resources/#criteria-for-outbound-sales-accepted-opportunity-sao)

- Who gets involved while evaluating a tool at {{company}}?
- Would you expect anyone to challenge your initiative, and if so can I help by connecting with anyonse else on your end?
- If you as a {{title}} wanted to purchase GitLab, what process internally would you have to follow, and how can we help you navigate it?
- What challenges do you expect to face when trying to pitch this change internally? Who has a say in this and what do they care about the most?

### Email Writing Cheat Sheet

The table below shows the Command of the Message Email Writing Checklist.

It aims to outline the structure of the emails we write to prospects. Emails should be written in each person’s language and tone and make sure to include the CoM frameworks as per the outline below. You can find additional resources [here.](https://docs.google.com/document/d/1-DF6bEtS9QF9idqBcK77RiLL04CKiFMuc0LDEM5N6RA/edit)

| Subject Line              |                                                   |
|---------------------------|---------------------------------------------------|
| Personalized              | - Use Prospect’s name, Company name, or colleague’s name. - Relate to their situation: Use recent company initiatives, technologies they use, Projects they have planned etc. |
| Curiosity and Urgency     | - Provide an image of their future situation: Guide the reader to imagine how their situation could change. - Use compelling events to give a clear image of where they are currently. |
| CoM                       | - Proof Points: Quote Case Studies or other customers. - How We Do it Better: Make use of our defensible differentiators. |
| Opening Line              |                                                   |
| Avoid cliches             | - Stand out: Avoid losing email real-estate with cliche phrases like “just following up” or “hope all is well”. - Brand yourself: Demonstrate that you have an understanding of the reader’s current situation and a clear idea of his required solutions. |
| CoM                       | - Before Scenarios: Paint a clear image of their current situation and how that is causing them professional or personal pain. - After Scenarios/PBOs: Tie the current situation with a clear image of a future situation that’s beneficial for the business and the individual. |
| Main Body                 |                                                   |
| Addressing Questions and Information provided | - No Free Consulting: Answer questions just enough to give a narrative to your email and tie into the CTA. - No Transactional Answers: Don’t make the reader feel like he’s interacting with an online form, a robot, or a sleazy salesman that doesn’t care. |
| CoM                       | - Discovery Questions: determine which discovery questions tie into their current situation, questions asked or information provided. - Trap-Setting Questions: if competitor technology or objections come up, use trap-setting questions to guide the reader into understanding our differentiators. |
| CTA                       |                                                   |
| Clear Next Step, Agenda and benefit | - Valuable: phrase your CTA in a way that’s clearly valuable to the reader and his specific situation. - Defined: outline what will happen in the next step and how long will it take |
| CoM                       | - Measurable: present metrics or PBOs that will be covered in the next step |

Additionally, you can use the matrix below as a quick-cheet to determine what datapoints can be used for personalization.

| Location               | Information                                                                                                         |
|------------------------|---------------------------------------------------------------------------------------------------------------------|
| LinkedIn               | - Have they recently been promoted? - What are the specific responsibilities stated on their page you are trying to help them with? |
| Company strategy from website | - What are the company’s strategic initiatives? (become more agile?, improve internal business systems?, focus on speed to market on delivery? Streamlining expenditure?) |
| Google search name of contact | - Do they have any blogs? - Have they featured in any articles? - Have they any personal achievements celebrated? - Do they have any side projects? |
| Keyword search company | - “COMPANY” AND (“Agile” OR “digital transformation” OR “DevOps” OR “business process improvement” OR “Cloud” OR “digital strategy”) |
| Leadership Focus       | - Find articles of leadership through boolean searches and ask propose how we can help them achieve these goals |
| Company News           | - Celebrate company achievements                                                                                   |
| Check support tickets  | - Are they experiencing issues that upgrading deployment options could help resolve?                               |
| Mutual Contacts        | - Do you have mutual contacts on LinkedIn you can talk about?                                                      |
| Use cases on website   | - Do they have the same customers as us? How do they help them?                                                    |
| Speak to a user of the tool to create personalized message to decision maker | - Speak to someone who uses out tools every day to uncover pain-points (performance degradation at peak times) and take that information to a decision maker |

## Making Changes to the handbook

[Video Walkthrough of how to make changes to the GitLab Handbook for the Sales Development org](https://www.youtube.com/watch?v=P7Nv7bzksiY&t=1032s)

One of our Values is being handbook first.

In order to align the SDR organization more closely to this ideal, below are suggested steps. Please remember that the Handbook is a living document, and you are strongly encouraged to make improvements and add changes. This is ESPECIALLY true when it comes to net new solutions that should be shared so the whole organization has access to that process. (aka The DevOps ideal of turning “Localized Discoveries” into “Global Knowledge”.)

Steps:

- Have a change you want to make to the handbook? Great!
- Navigate to the source code of the page in the handbook (e.g. Link to edit the SDR page in the Handbook )
- Click either “Edit” or “Web IDE” to make your changes.
- Have a brief but descriptive “Commit message” (e.g. “Add new section on ‘How to Make Changes to the Handbook’”) and commit your changes
- Fill out the Merge Request details

## Automatic Enrollment to Sequences

From FY24Q4 onwards, we will pilot [automatic triggers](https://support.outreach.io/hc/en-us/articles/221361788-How-To-Create-an-Outreach-Trigger) through Outreach.

### Automatic High PTP Sequence

As a first iteration, we will be automatically enrolling leads that indicate a high Propensity to Purchase (PTP) score to a full High Touch outreach sequence. The filters applicable for this trigger are:

- Lead has direct phone in their record
- Lead has a high PTP score (4 or 5)
- Lead is actively enrolled to a Low Touch Sequence

### Automatic Bounced Sequence

We have an additional process where leads that [were automatically moved to disqualify lead status because of a wrong email]https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/7718(), BUT have a phone number associated to them, will automatically be enrolled to a [sequence.](https://web.outreach.io/sequences/13842)

## BDR Territory Movement Rules of Engagement

When a territory moves from one BDR to another but the previous BDR remains with the same team (ex: moves from one MM territory to another) the BDR vacating the territory will have the chance to temporarily retain ownership of accounts with which they have generated ongoing engagement (subject to manager approval). At the time of the holdover, an issue will be created that will be reviewed in 30 days to verify account ownership. This includes:

- The outgoing BDR will be allowed to hold back for 30 days any account with which they can demonstrate two-way engagement that could potentially lead to an IQM and new opportunity. If, after 30 days, no IQM has been set and no stage 0 opportunity exists, the account will be moved by the manager to the new BDR assigned to the territory. If the outgoing BDR is successful in generating a new opportunity, the account will remain with them until the opportunity is either qualified or unqualified.
- The outgoing BDR will be allowed to hold back for 30 days any account with which there is an existing stage 0 opportunity where they are listed as the BDR. After 30 days if the opportunity is not qualified, the account will be moved by the manager to the new BDR assigned to the territory.
- In instances where an account has been reassigned to the new BDR and the outgoing BDR had two-way engagement more than 30 days ago with the account but was unable to convert them to an SAO, and the account later returns unprompted and with no prior activity from the new BDR assigned to the territory and wants to pursue a conversation with sales, the team (manager) will receive SAO credit from any resulting opportunity.
- Any other situations that arise which result in a credit dispute will be investigated and resolved by the BDRs’ leader and senior leader.

## Working with Resellers

The end-user account determines the BDR/SDR alignment: If you are the SDR assigned to the 3rd party lead, please gather the information in point 1 and pass the lead to the correctly assigned BDR (the BDR assigned to the end-user account) who will complete points 2-6

| Step                                           | Description                                                                                                                                                            |
|------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Gather billing and end-user details from the reseller | - Billing company name/address: <br> - Billing company contact/email address: <br> - End-user company name/address: <br> - End-user contact/email address: <br> - Snippet in outreach |
| Create a new lead record with end-user details | - Ensure that all notes are copied over to the new LEAD as this is the LEAD that will be converted.                                                                     |
| Converting the new lead                        | - Name opp to reflect reseller involvement as shown here: “End-user account name via reseller account name” <br> - Convert original reseller lead to a contact associated with the reseller account <br> - If an account does not already exist for the reseller, create one when converting the lead to a contact <br> - Assign the record to the same account owner <br> - Do NOT create a new opportunity with this lead. |
| Attach activity to the opportunity            | - On the reseller contact, go to the activity and link each activity related to your opportunity to the opp. <br> - Activity History > click edit to the left of the activity > choose ‘opportunity’ from the ‘related to’ dropdown > find the new opportunity > save |
| Update the opportunity                         | - Change the business type to new business and stage to pending acceptance. <br> - Under contacts, add the reseller contact, role as reseller, and primary contact. <br> - Under partners, add the reseller account as VAR/Reseller |

## SDR/BDR to AE SAO Handoff Process

The purpose of the BDR-AE outbound handoff process is three fold.

- Ensure Outbound prospects get the best experience possible.
- Ensure the BDR team enjoys operational efficiencies and have clearly structured ways to collaborate with the AE team on outbound accounts.
- Ensure the AE team is set up for success is receiving outbound leads in a structured manner and have the appropriate information to utilize in converting them.

To make sure that the hand-offs maximize internal efficiencies, it is required that:

- The BDR team makes sure to book calls, with a minimum notice time of 48 business hours.
- The BDR team makes sure to properly fulfil Outbound SAO criteria, or work with AE to create a mutual command plan.
- The AE team makes sure to:
  - Accept SAOs within 8 working hours after an IQM call.
  - To leave a chatter note on the opportunity record, tagging BDR and AE manager with feedback on the level of qualification and handoff in case of discrepancies.
  - To be responsible for managing the prospect relationship after all handoff types have taken place. This includes re-scheduling conflicts.

### 1. BDR Qualified Meeting

- Are leads that have been qualified by the BDR over a Discovery call.
- [CoM principles](https://docs.google.com/document/d/1m5YBOCc--M1Iq5-SEEd2OUWDjYyc6VJ3xTsDEEqisUQ/edit) have been applied to the call and some of the Before/After Scenarios, Positive Business Outcomes, Requirements and Metrics have been identified and agreed upon between the prospect and the BDR.
- There is a clear need or intent identified from the side of the company, and the prospect has clearly articulated a path to the ultimate decision-maker, as per the guidelines for outbound SAO criteria.

**BDR steps after discovery call**

- Summarize CoM principles uncovered during the call
- Schedule next step through Outreach while being on the call with the prospect
  - Meeting Type should be 45’ Evaluation Orchestration Call and body of invitation should be adjusted to meet the prospect’s needs.
- Send AE Intro Email
  - For demanding hand-offs, customer-facing agenda may also be copied and attached to intro email.
- Log Required SFDC fields and populate Notes field.
- Unless there’s a scheduling conflict for the BDR, attend Evaluation Orchestration Call and kick-off the call:
  - Summarize the BDR qualifying conversation by mentioning and verifying the before and after scenario of the prospect, as transcribed on the SAO notes.
  - After prospect acknowledges that their internal situation have not changes inbetween BDR and AE call, recap the expectations and metrics as transcribed on the SAO notes and handoff to AE to move forward.

#### 2. Joint IQM

- Are meetings booked with leads that are from pre-agreed upon Actively Working Acounts.
- Calls scheduled for these prospects will be taken as a joint IQM with both BDR and AE attending, and leading the qualification.
- CoM principles will be applied during the call, with the goal of uncovering a clear need and path to the ultimate decision-maker, as per the guidelines for outbound SAO criteria.

**BDR steps before discovery call**

- Schedule next step through Outreach while being on the call with the prospect, and schedule straight to the AEs calendar.
  - Meeting Type should be 15’ Discovery Call.
- Create SFDC opportunity, and log any pre-determined or relevant information from your past reseearch.
- Communicate with AE, and create a mutual command plan for the call.
  - When kicking off the call, summarize the BDR research and reason for reaching out, such compelling events, Persons with LIM, etc
  - After prospect acknowledges their compelling event and internal situation, discovery call continues with pre agreed upon structure with AE.

## Organizing a Group Demo

Organizing a shared demo makes it more simple for us to offer spontaneously a demonstration of GitLab, which should result in a easier handover of opps, and ease to create opps. As BDRs, we arranged the execution of this demo by creating a zoom webinar, and configuring the whole event so that the administrative side is automated.

| |                                                                                                                                                                                                                                                    |
|----------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Define the criteria that qualifies an opportunity as eligible to go to a shared demo.**                                                                                                                                                            |                                                                                                                                                                                                                                                    |
| SAL/AE is involved only                                                    | The SAL can invite his/her/their customers or contacts in order to add value to their ongoing discussion, if the AE/SAL finds it relevant. This is in the context of a conversation with the prospect/customer.                                                                                              |
| SAL/AE and BDR are involved                                                | The BDR schedules a discovery meeting with the SAL (whether it is a prospect or customer) that generates a qualified opportunity. Once the opportunity is created, the SAL/BDR suggests the shared demo instead of the individual demo based on the priority of the opportunity (importance of the account, number of licenses, amount of the opportunity, persona). This is in the context of a conversation with the prospect/customer. There needs to be an opportunity in this case to suggest the demo, for the sake of collaboration between the SAL and the BDR. |
| BDR is involved only                                                       | When a lead with decision power or significant influence in the org (no individual contributor) expresses a high interest for GitLab (aka: high read/open rates on an email, response received by phone with interest confirmed) AND when the BDR has a tangible reason to think that there is a need in the division/team/company (research, information obtained through other leads in the same org), the BDR can leverage the shared demo for HOT leads to create interest or a sense of urgency, the goal being to have the lead jump on an introductory meeting after the shared demo. |
| **Define the criteria for different groups of attendees who can join this demo**                                                                                                                                                                    |                                                                                                                                                                                                                                                    |
| Attendees coming from a BDR conversation                                    | In order to attend a demo, this attendee needs to be part of an opportunity in at least stage 1- Discovery.                                                                                                                                                                                                     |
| Attendees coming from the AE’s conversations                               | The AE can leverage this shared demo to invite his own contacts.                                                                                                                                                                                                                                                  |
| **Step-by-step guide to plan a demo**                                                                                                                                                                                                                  |                                                                                                                                                                                                                                                    |
| 1. Get a Zoom Webinars license                                              | You don’t have a Webinar licence by default, so you need to open an access request issue on GitLab to ask for one.                                                                                                                                                                                            |
| 2. Go on Zoom SSO > Webinars (on the left under Meetings) > Schedule a webinar | You will be able to save the entire configuration of the webinar as a template, in order not to set it up again.                                                                                                                                                                                               |
| 3. Change the topic, description, date/time, and duration                   | Change the topic (this will be the title of your demo, and the name of the event on Google Calendar), the description (describes the content of the demo, the idea is to adapt the focus of the demo depending on your attendees), set the date/time of the demo, and the duration (note that setting a certain duration will not stop the zoom webinar once the time has gone). |
| 4. Tick the box ‘Required’ next to registration                             |                                                                                                                                                                                                                                                    |
| 5. Change the Dial country to match attendees' language                      |                                                                                                                                                                                                                                                    |
| 6. Configure additional settings such as Q&A, recording, and email settings |                                                                                                                                                                                                                                                    |
| 7. Save the configuration as a template for future use                       |                                                                                                                                                                                                                                                    |

## Outbound Account Ranking Matrix

This complementary table can help structure your thinking around what outbound accounts to choose.

| Account Scoring & Prioritization |
|----------------------------------|
| **Priority 1 Actively Working Accounts (15%)** |
| - Strong ICP Qualities |
| - Compelling Triggers |
| - Strategy is Tailored and Targeted |
| - Future based next step dates |
| - Specific next step notes |
| **Priority 2 Actively Working Accounts (35%)** |
| - ICP qualities |
| - No Triggers |
| - Strategy is Targeted (persona/industry based) |
| - Future based next step dates updated weekly/bi-weekly |
| - Next step notes should just include week # |
| **Priority 3 Actively Working Accounts (50%)** |
| - ICP qualities |
| - No Recent Triggers |
| - Strategy is nurture based |
| - Future based next step dates updated monthly |
| - Next step notes should just include recent release # or event invite |
| **Other Good Account Scoring Qualifiers:** |
| - Current CE Usage |
| - 250+ employees in IT/TEDD positions |
| - Good Fit Industry / Vertical (High Growth, Technology, Financial, Healthcare, Regulated Business) |
| - Early Adopters / Innovative IT Shops (Identifiers & Keywords): Kubernetes / Containers, Microservices, Multi-cloud, DevOps, DevSecOps, CICD (and open-source + proprietary tools), SAST / DAST, Digital Transformation |
| - Current DevOps Adoption (multiple DevOps roles on staff or hiring for multiple DevOps positions) |
| **Outbound Prospecting Framework** |

| **Priority Level** | **Account Strategy Includes** | **Next Step Date** | **Next Step Notes** |
| Priority 1 | Priority #, Any Details | Future based next steps | Hyper-personalized, simultaneous outreach, creative, direct mail, ads, groundswell, events |
| Priority 2 | Priority #, Weekly Plan or Length of Sequence | Future based next steps depending on account strategy | Include the weeks outreach and overall strategy |
| Priority 3 | Priority # | Update Next Step Date to next months outreach | BDR Next Step, What kind of nurture (Product Updates, Events, Ect) |

## Growth Strategies, Ranking and RoE

**Strategies:**

- Growth within an existing customer account: BDR should strategically partner with SAE/ISR to identify additional business units to bring into the conversation as growth opportunities. Renewal dates should be exploited.
- Expansion into parent/children/subsidiaries of existing customer accounts: These are accounts that fall within the corporate hierarchy within Salesforce. If a child/subsidiary account is identified via ZoomInfo but not in our system, then BDR should follow the following process: Create a LEAD for that account and then convert to CONTACT to create the ACCOUNT when you qualify your first lead.
- Free to paid upgrades: Existing Core/CE users can be targeted to upgrade to a paid version of GitLab

**Ranking and prioritization of accounts:**

- Rank 1: Expand with Purpose (EwP) accounts should be closely/strategically worked with SAEs. These accounts have High 6Sense intent scores and should therefore be High touch by BDR.
- Rank 2: ICP Ultimate parent accounts that have Core/CE-Users in a hierarchy, Total CARR/LAM for account, Med/Low 6Sense intent scores, and small renewals in current FQ.
- Rank 3: As above but are not our ICP

**Professional Services Opportunities**

A Professional Services Opportunity will be used to cover any integration, consulting, training or other service that a Sales rep will sell to a prospect/client and needs or wants to be invoiced separately. To invoice separately a new quote and opportunity must be created.

## Frequently Used Terms

| Term                      | Definition                                                                                                                                                                  |
|---------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Accepted Lead             | A lead that an SDR or BDR agrees to work until qualified in or qualified out                                                                                               |
| Account                   | An organization tracked in salesforce.com. An account can be a prospect, customer, former customer, integrator, reseller, or prospective reseller                         |
| AM                        | Account Manager                                                                                                                                                             |
| AE                        | Account Executive, can be Major or Strategic in AMER/EMEA Enterprise                                                                                                       |
| APAC                      | Asia-Pacific                                                                                                                                                                |
| BDR                       | Business Development Represenative - focused on outbound                                                                                                                     |
| CAM                       | Channel Account Manager                                                                                                                                                    |
| CS                        | Customer Success                                                                                                                                                           |
| DACH                      | Germany, Austria and Switzerland region                                                                                                                                     |
| EMEA                      | Europe, Middle East, and Africa                                                                                                                                            |
| EULA                      | End User License Agreement                                                                                                                                                  |
| High intent               | An event, webcast, demo that is a strong indicator of purchase or evaluation                                                                                                |
| Inquiry                   | An Inbound request or response to an outbound marketing effort                                                                                                              |
| IQM                       | Initial Qualifying Meeting                                                                                                                                                  |
| LATAM                     | Latin America (includes all of Central & South America)                                                                                                                     |
| META                      | Middle East, Turkey and Africa Region                                                                                                                                       |
| MQL                       | Marketing Qualified Lead - an inquiry that has been qualified through systematic means (e.g. through demographic/firmographic/behavior lead scoring)                        |
| MVC                       | Minimal Viable Change (not Model View Controller)                                                                                                                           |
| NEUR                      | Northern Europe Region                                                                                                                                                      |
| NORAM                     | North America                                                                                                                                                               |
| Qualified Lead            | A lead that a BDR or SDR has qualified, converted to an opportunity and assigned to a Sales Representative (Stage 0-Pending Acceptance)                                     |
| RD                        | Regional Director                                                                                                                                                           |
| ROW                       | Rest of World                                                                                                                                                               |
| SAE                       | Strategic Account Executive                                                                                                                                                |
| Sales Admin               | Sales Administrator                                                                                                                                                         |
| Sales Serve               | A sales method where a quota bearing member of the sales team closes the deal                                                                                                |
| SAO                       | Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Initial Qualifying Meeting                                                                  |
| SDR                       | Sales Development Representative - focused on inbound                                                                                                                        |
| Self Serve                | A sales method where a customer purchases online through our web store. Also known as a “web direct”                                                                        |
| SEUR                      | Southern Europe Region                                                                                                                                                      |
| SLA                       | Service Level Agreement                                                                                                                                                     |
| SQO                       | Sales Qualified Opportunity                                                                                                                                                 |
| CSM                       | Customer Success Manager                                                                                                                                                   |
| TEDD                      | Technology, Engineering, Development and Design - used to estimate the maximum potential users of GitLab at a company                                                       |
| Won Opportunity           | Contract signed to Purchase GitLab                                                                                                                                          |
| Hyper-personalization    | The concept of combining real-time data extracted from multiple sources to create outreach that resonates with prospects on an individual level.                             |
| VIP (prospect)           | A Very Important Person, top officer, executive buyer, C-level prospect, or important influencer requiring hyper-personalization.                                            |
| Influencer (prospect)    | An individual prospect suspected to be involved with IT decision-making, tooling, teams, roadmap, strategic projects, and/or budgets.                                         |
| User (prospect)          | A prospect with limited influence within an IT organization.                                                                                                                 |
| Groundswell               | An outbound strategy focused on filling the top of the funnel by generating engagement, opt-ins, MQLs, and uncovering intent signals.                                         |
| Snippets                  | Content created for BDRs and SDRs to use to create hyper-personalized sequences, one-off emails, or to use for reaching out to prospects via LinkedIn.                      |
| Warm Calling              | The method used to strategically incorporate phone calls and voicemails into an outbound prospecting workflow.                                                               |
| Rules of Engagement (RoE) | Defines ownership of a lead, SAO credit, and collaboration between SDRs and BDRs.                                                                                          |
| BDR Prospecting Status    | Salesforce status includes Queued, Actively Working, Worked in FY, and Restricted.                                                                                         |
| Vartopia Partner Account | If populated, the lead/contact is worked by a partner, and Sales Dev should not proceed with outreach.                                                                     |
| Actively Working Status   | Accounts chosen for strategic outreach based on alignment, news, initiatives, etc. Recycles after 10 weeks if not worked.                                                    |
| Actively Working Status Requirements | BDR Account Strategy field must be populated. Research and Next Steps notes within 10 days. Minimum 5 people in sequences.                                               |
| Queued status             | Accounts waiting to be moved into Actively Working. SDRs work MQLs in this status.                                                                                          |
| Worked in FY status       | Indicates an account went through “Actively Working” this FY. Can be moved back to “Actively Working” later.                                                               |
| Restricted status          | SAE-indicated restriction on the account. BDRs handle the status change, note the reason, and reroute to assigned BDR.                                                      |

---

title: Sales Development Tools and Manager Resources
description: This page walks you through all the tools we use in the Sales Dev org.

---

## Sales Development Tools

This page is to act as your guide to the many tools and best practices that the Sales Development Organization uitlizes.

### ZoomInfo

[Zoominfo](https://www.zoominfo.com/) provides our Sales Development Representatives and Account Executives with access to hundreds of thousands of prospects and their contact information, company information, tech stack, revenue, and other relevant data. Individual records or bulk exports can be imported into Salesforce using extensive search criteria such as job function, title, industry, location, tech stack, employee count, and company revenue. More information can be found on the Marketing Operations [Zoominfo handbook page.](/handbook/marketing/marketing-operations/zoominfo/)

### ZoomInfo Training Resources

- ZoomInfo University: [40 Minute Introduction Video](https://university.zoominfo.com/learn/course/101-introduction-to-zoominfo-on-demand/training-session/101-introduction-to-zoominfo)
- GitLab Edcast: [ZoomInfo Introduction Training](https://gitlab.edcast.com/journey/week)
- GitLab Edcast: [ZoomInfo Advanced Training](https://gitlab.edcast.com/journey/week-note)
- ZI 101 Video: [Quick Saved Searches](https://www.youtube.com/watch?v=OpTgvoOQ1jM)
- ZI 101 Video: [Leveraging List Match](https://www.youtube.com/watch?v=vl1kpsNa874)
- ZI 101 Video: [Tagging Prospects in LinkedIn](https://www.youtube.com/watch?v=GQWTZWbMgg8)
- GitLab Handbook: [Zoominfo handbook page](/handbook/marketing/marketing-operations/zoominfo/)

## LinkedIn Sales Navigator

[LinkedIn Sales Navigator](https://www.linkedin.com/sales/login) is a prospecting tool & extended reach into LinkedIn Connections.

### LinkedIn Training Resources

- LinkedIn Intro: [70 Minute Navigator Tutorial](https://www.linkedin.com/learning/learn-linkedin-sales-navigator-3/welcome-to-sales-navigator)
- GitLab Edcast: [LinkedIn Navigator Training](https://gitlab.edcast.com/journey/week-note)
- GitLab Video: [Peer Tips](https://drive.google.com/file/d/1xzz8cEiSFqZ7bOw-dpqoNlHjSDwrIFE4/view)
- GitLab Video: [LinkedIn Personalization](https://www.youtube.com/watch?v=7vEVB-WgDPA)
- GitLab Video: [Loading Accounts from Salesforce into LinkedIn](https://www.youtube.com/watch?v=_D8walmmOAU)

## 6Sense

6Sense is a targeting and personalization platform that we use to reach our different audiences based on intent data and our ideal customer profiles. [6Sense Handbook Page](/handbook/marketing/marketing-operations/6sense/)

### 6Sense Training Resources

- [General 6Sense Resources](https://6sense.com/resource-library/)
- [6Sense for BDRs Video](https://6sense.com/resources/videos/inside-sales-persona-demo)
- [6Sense Account Identification Guide](https://6sense.com/platform/account-matching/the-guide-to-account-identification/)
- [6Sense Persona Identification Guide](https://6sense.com/guides/how-to-identify-and-influence-the-entire-buying-team/)

### 6Sense 6QA Automations

We currently have in place an automation that will automatically move accounts that reach 6QA status from 6Sense to SFDC.

Specifically:

- We have three segments (SMB, MM, ENTG) saved on folder 9a of 6Sense. Each of these segments tries to identify the top accounts we currently have on SFDC that are not being worked by our team.
- Through 6Sense Orchestrations, we will automatically update these accounts weekly.
  - We aim for 200 records to be enriched every week.
  - We aim for a 90 day buffer period between records being enriched.
  - The SFDC field `Sales Dev Automation` will be enriched with the value of the respective segment (SMB, MM, ENTG)
- From the moment these accounts get enriched by 6Sense, they will automatically show up on our pre-saved searches on ZoomInfo, one for each segment.

  - [SMB Saved Search](https://app.zoominfo.com/#/apps/searchV2/v2/results/person?query=eyJmaWx0ZXJzIjp7InBhZ2UiOjEsImNvbXBhbnlQYXN0T3JQcmVzZW50IjoiMSIsImlzQ2VydGlmaWVkIjoiaW5jbHVkZSIsInNvcnRCeSI6IlJlbGV2YW5jZSIsInNvcnRPcmRlciI6ImRlc2MiLCJleGNsdWRlRGVmdW5jdENvbXBhbmllcyI6dHJ1ZSwiY29uZmlkZW5jZVNjb3JlTWluIjo4NSwiY29uZmlkZW5jZVNjb3JlTWF4Ijo5OSwib3V0cHV0Q3VycmVuY3lDb2RlIjoiVVNEIiwiaW5wdXRDdXJyZW5jeUNvZGUiOiJVU0QiLCJleGNsdWRlTm9Db21wYW55IjoidHJ1ZSIsInJldHVybk9ubHlCb2FyZE1lbWJlcnMiOmZhbHNlLCJleGNsdWRlQm9hcmRNZW1iZXJzIjp0cnVlLCJzY29yaW5nT3B0aW9ucyI6IntcImlkXCI6XCJaUmhSeWJtUHIxMGU2cEVQVDVBcVwiLFwib25seUNvbnRhY3RGaWx0ZXJzXCI6ZmFsc2V9Iiwic291cmNlSWQiOiJBTlVSQSIsInRpdGxlU2VuaW9yaXR5IjoiQ19FWEVDVVRJVkVTLFZQX0VYRUNVVElWRVMsRElSRUNUT1IiLCJycHAiOjI1LCJkZXBhcnRtZW50cyI6IkluZm9ybWF0aW9uIFRlY2hub2xvZ3ksRW5naW5lZXJpbmcgJiBUZWNobmljYWwsQy1TdWl0ZSIsImhhc01vYmlsZVBob25lIjoiaW5jbHVkZSIsIkNGLXNhbGVzZm9yY2UtYWNjb3VudC0xMDEiOnsiaW5jbHVkZSI6eyJwbGF0Zm9ybSI6InNhbGVzZm9yY2UiLCJlbnRpdHkiOiJhY2NvdW50IiwiZmllbGROYW1lIjoiMTAxIiwiaWRMaXN0IjpbIjQwNTk5eDN6bHRmeDliNWoiXSwiaXNJbmNsdWRlIjp0cnVlfX19LCJzZWFyY2hUeXBlIjowfQ%3D%3D)
  - [MM Saved Search](https://app.zoominfo.com/#/apps/searchV2/v2/results/person?query=eyJmaWx0ZXJzIjp7InBhZ2UiOjEsImNvbXBhbnlQYXN0T3JQcmVzZW50IjoiMSIsImlzQ2VydGlmaWVkIjoiaW5jbHVkZSIsInNvcnRCeSI6IlJlbGV2YW5jZSIsInNvcnRPcmRlciI6ImRlc2MiLCJleGNsdWRlRGVmdW5jdENvbXBhbmllcyI6dHJ1ZSwiY29uZmlkZW5jZVNjb3JlTWluIjo4NSwiY29uZmlkZW5jZVNjb3JlTWF4Ijo5OSwib3V0cHV0Q3VycmVuY3lDb2RlIjoiVVNEIiwiaW5wdXRDdXJyZW5jeUNvZGUiOiJVU0QiLCJleGNsdWRlTm9Db21wYW55IjoidHJ1ZSIsInJldHVybk9ubHlCb2FyZE1lbWJlcnMiOmZhbHNlLCJleGNsdWRlQm9hcmRNZW1iZXJzIjp0cnVlLCJzY29yaW5nT3B0aW9ucyI6IntcImlkXCI6XCJaUmhSeWJtUHIxMGU2cEVQVDVBcVwiLFwib25seUNvbnRhY3RGaWx0ZXJzXCI6ZmFsc2V9Iiwic291cmNlSWQiOiJBTlVSQSIsInRpdGxlU2VuaW9yaXR5IjoiQ19FWEVDVVRJVkVTLFZQX0VYRUNVVElWRVMsRElSRUNUT1IiLCJycHAiOjI1LCJkZXBhcnRtZW50cyI6IkluZm9ybWF0aW9uIFRlY2hub2xvZ3ksRW5naW5lZXJpbmcgJiBUZWNobmljYWwsQy1TdWl0ZSIsImhhc01vYmlsZVBob25lIjoiaW5jbHVkZSIsIkNGLXNhbGVzZm9yY2UtYWNjb3VudC0xMDEiOnsiaW5jbHVkZSI6eyJwbGF0Zm9ybSI6InNhbGVzZm9yY2UiLCJlbnRpdHkiOiJhY2NvdW50IiwiZmllbGROYW1lIjoiMTAxIiwiaWRMaXN0IjpbIjQwNTk5eDN6bHRmeDliNWkiXSwiaXNJbmNsdWRlIjp0cnVlfX19LCJzZWFyY2hUeXBlIjowfQ%3D%3D)
  - [ENTG](https://app.zoominfo.com/#/apps/searchV2/v2/results/person?query=eyJmaWx0ZXJzIjp7InBhZ2UiOjEsImNvbXBhbnlQYXN0T3JQcmVzZW50IjoiMSIsImlzQ2VydGlmaWVkIjoiaW5jbHVkZSIsInNvcnRCeSI6IlJlbGV2YW5jZSIsInNvcnRPcmRlciI6ImRlc2MiLCJleGNsdWRlRGVmdW5jdENvbXBhbmllcyI6dHJ1ZSwiY29uZmlkZW5jZVNjb3JlTWluIjo4NSwiY29uZmlkZW5jZVNjb3JlTWF4Ijo5OSwib3V0cHV0Q3VycmVuY3lDb2RlIjoiVVNEIiwiaW5wdXRDdXJyZW5jeUNvZGUiOiJVU0QiLCJleGNsdWRlTm9Db21wYW55IjoidHJ1ZSIsInJldHVybk9ubHlCb2FyZE1lbWJlcnMiOmZhbHNlLCJleGNsdWRlQm9hcmRNZW1iZXJzIjp0cnVlLCJzY29yaW5nT3B0aW9ucyI6IntcImlkXCI6XCJaUmhSeWJtUHIxMGU2cEVQVDVBcVwiLFwib25seUNvbnRhY3RGaWx0ZXJzXCI6ZmFsc2V9Iiwic291cmNlSWQiOiJBTlVSQSIsInRpdGxlU2VuaW9yaXR5IjoiVlBfRVhFQ1VUSVZFUyxESVJFQ1RPUiIsInJwcCI6MjUsImRlcGFydG1lbnRzIjoiSW5mb3JtYXRpb24gVGVjaG5vbG9neSxFbmdpbmVlcmluZyAmIFRlY2huaWNhbCIsImhhc01vYmlsZVBob25lIjoiaW5jbHVkZSIsIkNGLXNhbGVzZm9yY2UtYWNjb3VudC0xMDEiOnsiaW5jbHVkZSI6eyJwbGF0Zm9ybSI6InNhbGVzZm9yY2UiLCJlbnRpdHkiOiJhY2NvdW50IiwiZmllbGROYW1lIjoiMTAxIiwiaWRMaXN0IjpbIjQwNTk5eDN6bHRmeDliNWMiXSwiaXNJbmNsdWRlIjp0cnVlfX19LCJzZWFyY2hUeXBlIjowfQ%3D%3D)

- These saved searches will pull the relevant decision-makers per segment, and through ZoomInfo workflows do the following:

  - Automatically enroll SMB leads to the fully automated sequence [here](https://web.outreach.io/sequences/13896/overview)
  - Automatically add MM/ENTG accounts to the [SFDC dashboard here.](https://gitlab.my.salesforce.com/01ZPL000000kAPN)
  - Automatically add a sample size of prospects from each 6QA'd account to the SFDC view titled `B6 - My 6QA Leads.`

## Qualified

[Qualified](https://www.qualified.com/) is the Sales Dev Org's tool we use to chat with visitors on the GitLab website. Currently the SDR teams primarily respond to inbound chats and visitors using Qualified. The BDRs also have the ability to initiate chats with known leads from their actively working accounts.

## Chorus

Call and demo recording software. [Chorus](https://www.chorus.ai/) tracks keywords, provides analytics, and transcribes calls into both Salesforce and Outreach. Chorus will be used to onboard new team members, provide ongoing training and development for existing team members, provide non-sales employees with access to sales calls, and allow sales reps to recall certain points of a call or demo. At this time, US Sales, US Customer Success, US SDRs will be considered recorders. Non-US Commercial and Non-US SDRs can request recorder access once they have completed the GDPR training course. Non-US recorders will also need access to the EMEZ Zoom Group. Everyone else can access the tool as a listener if they wish.

### Chorus Training Resources

- Chorus: [Tips for Getting Start](https://docs.chorus.ai/hc/en-us/articles/115009183547-Tips-on-Getting-Started-with-Chorus)
- GitLab Edcast: [Chorus Overview](https://gitlab.edcast.com/journey/week)
- GitLab Edcast: [Sample Chorus IQM Calls](https://gitlab.edcast.com/insights/sample-chorus)

## Crayon Competitive Messaging Resources

[Crayon](https://app.crayon.co/intel/gitlab/battlecards/) hosts competitive messaging resources that are maintained by GitLab's product marketing team.

This is where you can find messaging related to GitLab vs some of our competiors such as GitHub, Atlassian, and Azure DevOps

## Sales Dev Manager Resources

### FY24Q3 Manager Tool Certification

We have an end-to-end process and tool walkthrough that walks manager through all the knowledge and information they need to know to be able to use our tech stack, both for inbound and outbound processes. The full walkthrough of this training is [documented on the issue here](https://gitlab.com/gitlab-com/marketing/sales-development/-/issues/401). Moreover, abbreviated notes of the training can be [found here](https://docs.google.com/document/d/1chOoHvwk-dSsAwoloZhdGiJJoFz0yjjq07kOYmzQUvQ/edit).

### General leadership principles

This page will serve as a training resource and operational guide for current and future managers. All Sales Development Managers should follow the [general leadership
principles](/handbook/leadership/) set out in the handbook.

- [1-1s](/handbook/leadership/1-1/)
- [Providing regular feedback](/handbook/leadership/#giving-feedback)
- [Dealing with underperformance](/handbook/leadership/underperformance/)

### Manager Onboarding

Onboarding is essential for all Sales Development Managers at GitLab. As part of onboarding, a [Becoming a GitLab Manager](https://gitlab.com/gitlab-com/people-group/Training/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md) issue will be created for each manager when they join, or are promoted. This issue is intended to connect new managers with the crucial information they need, and ensure they have access to all the resources and training available.

### Outbound BDR Process Manager Onboarding

The BDR process that we have defined here in GitLab is meant to provide a repeatable set of steps that an outbound BDR can follow to achieve results. It is very important for an onboarding manager to align themselves as quickly as possible to this process as it is a proven method that will help them manage their team in a efficient and data-driven way.

The BDR Process is explained step-by-step in the [main SDR handbook page here](/handbook/marketing/revenue-marketing/sdr/#bdr-outbound-process)

For a newcoming manager, we provide the [Manager attention needed boards](/handbook/marketing/sales-development/sales-development-tools/#tracking--monitoring-resources) that will be your main tool in understanding where your team is aligned to our BDR process, and where they need assistance.

To help structure your usage of the above, you can follow the steps below during your first month at GitLab while [the document here will be your main go-to resource](https://docs.google.com/document/d/18k1_jDHVgNzy2SzcPZ7GAOdJ6zb9i37Z4-V1-dEFKiU/edit#)

| Action | Benefit |
| ------ | ------- |
| Clone the Action Needed Dashboard and edit each report to be exclusive to your team's names | Gives you a SSoT that you and your team can easily reference |
| Review the dashboard with your team, and discuss how the data on it connect to the [BDR KPIs](/handbook/marketing/revenue-marketing/sdr/#sdrbdr-roe-and-inbound-lead-management) | Allows you to understand your team's level of maturity and each team member's current level of alignment to existing processes |
| Take note of any discrepancies or points of feedback from the team, either transcribe them to 1:1s for individual conversations or to the SDR Issue board for org-wide improvements | Enables you to filter between discrepancies that are caused because of a team member's lack of diligence that should be improved upon by the individual OR for discrepancies that were caused by an org-wide operational shortcoming that should be improved upon on a global scale. |
| Set realistic expectations with the team about adherance to org KPIs and set a review mechanism to go over them on a reccuring basis | Helps maintain a repeatable structure of accountability for your entire team |

### General Sales Development Leadership Resources

| Resource | Purpose |
| :----: | :----: |
|  [Leadership Handbook](/handbook/leadership/) | Tools and resources to assist people managers in effectively leading and developing team members at GitLab |
|  [Compensation Review Cycle (Compa Review)](/handbook/total-rewards/compensation/compensation-review-cycle/) | How to review our Compensation Calculator and carry out the Compensation Review Cycle |
|  [Sales Dev Manager Onboarding Checklist](https://docs.google.com/spreadsheets/d/1cnKc757SWenlJRoa_zqPhFyekp4mq56oSMFXPSDW-CU/edit#gid=0) | Make a copy and complete this checklist to ensure you know your team's tools and processes |
|  [360 Feedback](/handbook/people-group/360-feedback/) | Opporunity for managers, direct reports, and cross functional team members to give feedback to each other. Schedule and all information on this page.|
|  [Workday](/handbook/people-group/360-feedback/) | All team member HR information |
|  [Transitioning to a Manager Role at GitLab](/handbook/people-group/learning-and-development/manager-development/) | New manager resources and what to expect |

## Lead Routing & Alignment Resources

| Resource | Purpose |
| :----: | :-----: |
|  SSoT Sales > Sales Development Territory Alignment Sheet  | Can be found in Manager Home Base sheet. Single source of truth document for Sales Development to AE/SAE/Territory Alignment |
|  [Territory Change Request Issue Board](https://gitlab.com/gitlab-com/marketing/sales-development/-/boards/5569691) | Use the territory Change Request Issue Board and the BDR_Territory_Change Sales Development Issue Template in order to request a territory change for your rep.  |
|  [Sales Dev Internal Onboarding and Transition issue template](https://gitlab.com/gitlab-com/marketing/sales-development/-/blob/main/.gitlab/issue_templates/Internal_Onboarding_and_Transition_Template.md) | This issue template is to be used when a new teamember is joining the Sales Development Org for the first time or they are transitioning from SDR to BDR or vice versa. |
|  [BDR Territory Change Request issue template](https://gitlab.com/gitlab-com/marketing/sales-development/-/blob/main/.gitlab/issue_templates/BDR_Territory_Change.md) | Use this issue template when you would like to request territory changes for your BDRs. |
|  [Sales Dev Exit Handover Template](https://gitlab.com/gitlab-com/marketing/sales-development/-/blob/main/.gitlab/issue_templates/BDR_Territory_Change.md) | This template is meant to be used for team members that are departing from a team to hand off their pending tasks to their peers. |

## GitLab Resources

|                                                                                             Resource                                                                                             | Purpose |
|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:| :-----: |
|                                         [Adding yourself or someone else to the team page](/handbook/editing-handbook/#add-yourself-to-the-team-page)                                          | Video to assist new hires with updating their blank team page placeholder |
|                                                                             Update manager or SDR role in Salesforce                                                                             | To update a manager or Sales Development role in Salesforce, submit a [single person access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) or [bulk access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Bulk_Access_Request) depending on the number of roles that need to be updated. |
|                                                                          Create or update members of a Slack user group                                                                          | A user group is a group of members in a workspace who often need to be notified at once — for example, @managers. To update who is in one of the Sales Development groups, submit a [single person access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) or [bulk access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Bulk_Access_Request) depending on the number of people that need to be added. Fill out the requested info and delete any remaining info that isn't needed. Under 'Account Creation' put Slack User Group: @ Name (i.e. @Managers). You can also use the bulk AR to request the creation of a user group and list the users who should be in it.  |
|                                                                                Add someone to the Sales Development Gmail alias                                                                                | Submit a [single person access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) or [bulk access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Bulk_Access_Request) depending on the number of people that need to be added. Fill out the appropriate info and delete any remaining info that isn't needed. Under 'Account Creation' put the Sales Development email alias |
|                                                         [Make an edit to the handbook](/handbook/editing-handbook/)                                                          | Guide for how to edit the handbook. *Note: all new hires must do this as part of their onboarding |
|                                                          [Add a new page to the handbook](https://www.youtube.com/watch?v=9NcJG9Bv6sQ)                                                           | This GitLab Unfiltered video will walk you through how to create a new handbook page |
|                                            [Create a new job family](/handbook/hiring/job-families/#job-family-creation-using-web-version-of-gitlab)                                             | For each job at GitLab, the job family is the single source of truth for the expectations of that role. If you need information about when to create a new job family vs when to use an existing one watch [this video](https://www.youtube.com/watch?v=5EcFz1qNj2E&feature=emb_title) |
|                                           [Resolve failed pipeline when creating an MR](https://www.youtube.com/watch?v=WlgH-6cX1k8&feature=youtu.be)                                            | Quick overview of how to go about identifying why a pipeline might be failing for a merge request to the handbook page |
| [Sales Development Onboarding Job Specific Task Section](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/main/.gitlab/issue_templates/onboarding_tasks/department_sales_development.md) | This task section will automatically be added to the general onboarding issue for new SDRs based on their role when hired. |

## Tracking & Monitoring Resources

| Resource | Purpose |
| :----: | :-----: |
|  [SDR Issue Board](https://gitlab.com/gitlab-com/marketing/sales-development/-/boards/5569691)  | Used to track GitLab issues involving the SDR team. This is a global issue board. |
|  [SDR Event Tracker Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1718115) | Used to follow upcoming events globally |
|  [SDR Sisense Dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DraftSalesDevAnalyticsHub/Overview?:iid=1) | SalesDev Analytics Hub Dashboard  |

|  [Lead View Descriptions](/handbook/marketing/sales-development/#lead-and-contact-views) | There are Manager Lead views in SFDC mirroring the SDR and BDR views which are described on the linked Handbook page on the left. These views need to be checked regularly by managers to ensure all necessary leads are being worked.

## Action Needed Boards

| Teams | Boards |
| :----: | :-----: |
|  **AMER BDR TEAMS**  | [AMER BDR Action Needed](https://gitlab.my.salesforce.com/01ZPL00000069v3) -  [AMER BDR Manager Attention Needed](https://gitlab.my.salesforce.com/01ZPL00000069VF) |
|  **EMEA BDR TEAMS**  | [EMEA BDR Action Needed](https://gitlab.my.salesforce.com/01Z4M000000soBO) -  [EMEA BDR Manager Attention Needed](https://gitlab.my.salesforce.com/01ZPL0000006ABB) |
|  **APAC BDR TEAMS**  | [APAC BDR Action Needed](https://gitlab.my.salesforce.com/01ZPL000000bkbl) -  [APAC BDR Manager Attention Needed](https://gitlab.my.salesforce.com/01ZPL000000cUJd) |
|  **Global SDR Inbound Teams**  | [SDR Global Action Needed](https://gitlab.my.salesforce.com/01Z4M000000soBT) - [SDR Global Manager Attention Needed](https://gitlab.my.salesforce.com/01ZPL000000pryL)  |

## Sales_Dev_fyi Channel Information

### Post Naming Convention

- Posts in #Sales_Dev_fyi should have a header that lets the Reps know who the post is for, what type of post it is, and the urgency of the post.
- It is requested  when a post in the Sales Dev FYI channel pertains to you that you leave a 👀  emoji on the post to let management know that you read it

Format: **Audience | Type | Urgency**
Example:

- **Audience**
  - All of Sales Development
  - All SDRs
  - All BDRS
  - Specific Teams/Positions
    - Ex. AMER Large Land East

- **Type**
  - Enablement - (Mandatory, Optional)
  - Operations -  (Process Change, Tools, Sequences, Reports, System Changes/System Updates )
  - New Event/Initiative/Resource
  - Survey
  - Org Wide Announcement

- **Urgency**
  - 🚨 Action Required - urgent, action required with a due date
    - Example: Mandatory enablement launch
  - 🧠 Need to Know - urgent, update directly impacts audiences' workflows/processes
    - Example: Sales Dev process change
  - 📊 Feedback Requested - less urgent, action requested (with or without long-lead due date) but not required
    - Example: Survey from Product Group
  - 👀 Review - less urgent, does not directly or materially impact audiences' workflows/processes
    - Example: New customer story or new competitive resource

- **Example Sales_Dev_fyi channel post titles**
  - `[All of Sales Development] | [Enablement - Mandatory] | [🚨 Action Required]`

  - `[All BDRs] | [Operations - Outreach Process Cleanup] | [🧠 Need to Know ]`

  - `[EMEA Enterprise Land] | [Operations - New Outreach Event Sequence] | [🚨 Action Required ]`

  - `[All of Sales Development] | [Survey - People Group Survey Reminder] | [📊 Feedback Requested ]`

## Field Marketing <> BDR Collaboration Process

Our FM/BDR collaboration process is a method that we follow in the spirit of maximizing cross-functional collaboration. We have created an issue template for Field Marketers to populate, which in turns gets managed from the [Kanban Board here.](https://gitlab.com/gitlab-com/marketing/sales-development/-/boards/5665082) The premise of the [issue template](https://gitlab.com/gitlab-com/marketing/sales-development/-/blob/main/.gitlab/issue_templates/FM_BDR_Collaboration_Template.md) and board is to streamline communication between our teams. The issue template very clearly articulates all next steps required of this process while it also tries to leave space for us much AdHoc collaboration as required for each specific event.

As a BDR Director you will be tagged first in this issue as the primary DRI, while as a BDR Manager you will be involved in this issue on a case-by-case basis by your regional Director. All next steps of this process are clearly mentioned in the template for the issue so please follow each step in turn. The Sales Dev Operations team is also tagged and will be there to monitor progress and offer help if requested.

## Status Related 6Sense Reports

On folder #5 in the [6Sense segments list](https://gitlab.abm.6sense.com/segments/manage), you'll find the templates for the main three categories of accounts that we have. These templates are meant to be cloned and edited for your teams as per the guidelines below and will assist with coaching on 1:1s and team calls, as well as driving more predictable pipeline building.

**[Currently Actively Working Accounts Template](https://gitlab.abm.6sense.com/segments/segment/495284/accounts/?mode=edit&segment_type=6sense_network)**

- This report will cross-reference your team's currently AWAs against 6Sense intent data, and will highlight the best ICP accounts currently on your team's pipeline.

**[CE/SFDC Accounts not in Actively Working Status](https://gitlab.abm.6sense.com/segments/segment/495296/accounts/?mode=edit&segment_type=6sense_network)**

- This report will highlight the best ICP accounts on our existing database, that are not actively being pursued by your team.

**[Greenfield Accounts not in Actively Workin](https://gitlab.abm.6sense.com/segments/segment/495295/accounts/?mode=edit&segment_type=6sense_network)**

- This report will highlight the best ICP accounts that are NOt currently on our existing database. Please note that due to the complexity of our sales territories, there may be edits required on filter #8 (Address). There are multiple variants for City, Country or Region that can be used to accurately display each team's territory. Please feel free to reach out to the Ops team to help you determine these and generate the best reports possible.

## Sales Dev Territory and Role Reports

**[Sales Dev by Salesforce Profile and Role -](https://gitlab.my.salesforce.com/00O8X000008gonx)**

- Sales Dev Ops profile is the only profile with the ability to help add a member to opps beyond stage 0 (we can't add SDR/BDR to closed won opps however)
- Certain roles have certain permissions ie. Team Lead roles are able to transfer leads. Director level roles are able to help when account assignments are incorrect by updating account address and employee size information.
- Roles determine visibility based on the region (AMER, APAC, EMEA). Example: a BDR with EMEA in their role, will be able to see all accounts where the account territory owner has EMEA in their territory role name.

**[Sales Dev Territories by Team role/member associated with each territory -](https://gitlab.my.salesforce.com/00O8X000008gpBL)**

- This allows you to see what members/roles are associated with each territory. When you need a change in BDR assignment to a territory, please sharethe territory name with Sales Dev Ops, not the name of the account. Search for the account owner name and tell Sales Dev Ops which territory/territories require that BDR to be assigned.

## New Account AE Reports

- Use [this report](https://gitlab.my.salesforce.com/00O8X000008gmAA) to figure out the territory name for the account you are working. Use command F to find the zip/state/country and figure out correct territory name.
- Then use [this report](https://gitlab.my.salesforce.com/00O8X000008glPn) by plugging in the territory name to figure out who the correct AE is
- Please make sure you have all the correct location information on the lead you converting to initially create this new account.

## Onboarding

GitLab People Connect Team members will [create the onboarding issue](https://internal.gitlab.com/handbook/people-group/people-operations/people-connect/onboarding_process/#timing) and start completing the onboarding tasks, no later than one week before the new team member joins. People Connect Team members require a minimum of 4 business days (with the new hire timezone as the basis) before the new hire's start date to complete all onboarding tasks. This issue will be automatically assigned to you. As a manager, you will [also have tasks](/handbook/people-group/general-onboarding/#managers-of-new-team-members) that need to be completed prior to the new team member's start date.

The general onboarding issue will also automatically add a ['Sales Development' section](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/main/.gitlab/issue_templates/onboarding_tasks/department_sales_development.md) under 'Job Specific Tasks' based on the role of the new SDR. Both you and your new hire will have tasks to complete in this section.

With the creation of this issue, an [access request (AR) will also be automatically created](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_sales_development) for a new team member on their second day at GitLab. This AR lists the role based entitlements (pre-defined groups and system-level access that are granted automatically to team members depending on their role) your new hire will need.
     *See what is being auto provisioned on this AR [here](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_demand_generation).

### New Hire's First Day

On your new hire's first day, the assigned People Connect Team member will [schedule a welcome email](https://internal.gitlab.com/handbook/people-group/people-operations/people-connect/onboarding_process/#day-1-onboarding-tasks) to arrive at 7:30am (local time of the new team member) on their start date detailing how your new hire can access GitLab and begin their onboarding process.

### Manager Onboarding Checklist

##### Prior to Day 1

- Complete ‘Manager’ tasks on the onboarding issue *Note: there are tasks to complete prior to your new hire starting
- Schedule a welcome call at the start of your new hire’s first day to discuss:
  - Main focus should be completing as much of your onboarding as possible.
  - When you have available time feel free to move ahead as some onboarding task lists for the day won't take you all day.
  - How to manage meeting invites in your inbox (there are a ton!)
  - You will receive numerous emails in their first two weeks asking you to register or activate your license to a specific tool we use, please go ahead and do all of this.
  - What you can expect in regards to [onboarding at GitLab as an SDR](/handbook/marketing/sales-development/sdr-playbook-onboarding/)

##### After new hire has started

- Complete remaining ‘Manager’ tasks on the onboarding issue
- Set up [1:1s](/handbook/leadership/1-1/)

## Career Mobility Issues

A career mobility issue should be opened 2 weeks before the transition date by the people connect team. If the aligned manager does not see that issue created 2 days before the scheduled transition date, the manager should reach out to the People Connect Team via the [#people-connect Slack Channel](https://gitlab.slack.com/archives/C02360SQQFR).

**People connect opens mobility issue if any of the following are true:**

1. If there is a change in department.  Ex. SDR/BDR moves to the SMB Sales Team
2. Someone changes from Individual Contributor to Manager
3. Someone changes from Individual Contributor to Team Lead
4. Someone Changes from Manager to Individual Contributor
5. Someone Changes Teams

## Leave of Absence

If an SDR will be out for a prolonged period of time, please follow the proper processes and complete the SDR leave checklist.

- [Parental Leave](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave)
- [Emergency Situations](/handbook/people-group/paid-time-off/#communicating-time-off-for-emergency-situations)
- [Unpaid leave](/handbook/people-group/paid-time-off/#unpaid-leave-of-absence)

### Manager Sales Development Leave Checklist

-

## Offboarding

The full process for offboarding at GitLab differs based on whether it is voluntary or involuntary. These processes can be found on the [offboarding handbook page](/handbook/people-group/offboarding/#offboarding).

### Manager Offboarding Checklist

- The people team will create an [offboarding issue](/handbook/people-group/offboarding/#offboarding), complete all ‘Manager’ tasks on the issue. If you have any questions use the #managers #people-connect slack channels. You can also reach out to the assigned people ops team member on the issue.

### Sales Dev Handover Issue

- When a teamember is either leaving GitLab, leaving the Sales Dev Org, or transferring to another BDR team the departing team's manager must created a [Sales Dev Handover Issue](https://gitlab.com/gitlab-com/marketing/sales-development/-/blob/main/.gitlab/issue_templates/Exit_handover_template.md?ref_type=heads)

## Sequence Creation

- We follow the process outlined [here](/handbook/marketing/sales-development/how-tos/#new-outreach-sequenceworkflow-creation) for creating new content, when it comes to reviewing this content, the Sales Dev Ops team will follow a quarterly cadence of reviewing the sequences/campaigns that are on the In-flight column on the board [here](https://gitlab.com/gitlab-com/marketing/sales-development/-/boards/5540104). We use the criteria below:

### Sequence needs to be a globally applicable

- Only sequences that could be used by any team member globally can be added to the Good Collection.
- If a regionalized sequence can be translated to English, then the English copy of it will be added to the GC while the local variant will be added to the Team Collection.
- Field Marketing event sequences and any other similar type will not be considered for the Good Collection.

### Sequence needs to have completed the requirements set out on the issue descrption

- At least 200 people enrolled and/or having completed the sequence on a timely manner.
- ( Sequence has over 5% reply rate AND No less than 50% negative sentiment rate. ) OR Sequence has over 5% meeting set rate.

### Things to look out for when reviewing a sequence as a Manager

- Does the messaging of the sequence follow Command Of the Message?
  - Are all emails formatted appropriately with pain-probing questions and value propositions?
  - Are the relevant value drivers for the personas targeted by this sequence used?
- Is there a proper mix of automated and manual touches?
  - Are 1-2 highly personalized emails used for High-Touch sequences?
  - Are there at least 4 call steps for High-Touch sequences?
  - For Low-Touch/Awareness campaigns are all touchpoints automated?
- Are proper Outreach elements used?
  - Are automated variables used all on all emails?
  - Are manual personalisation variables used on manual emails?
- Is the purpose of the sequence clearly articulated on the issue description?
  - Are there other sequences that are proven to work that could be used instead?
  - Is there a valid reason for this campaign? Will it contribute to results in terms of SAOs or awareness?
  - Is the target demographic clearly thought out?

## Sales Development Onboarding

As BDRs and SDRs, you work closely with the larger marketing org as well as the sales team. Due to this, your onboarding will encompass training on tools and processes related to not only your role but a blend of marketing and sales.

This learning experience involves virtual self-paced learning paths in the form of a general company-wide onboarding issue, a Sales Development specific issue, as well as a Google Classroom course to prep you for [Sales Quick Start (SQS)](/handbook/sales/onboarding/SQS-workshop/). SQS is a 3-day in-person immersive and hands-on workshop for all new hires in a sales development or sales role. In parallel, you will meet regularly with your manager, onboarding buddy and members of our sales development enablement team to chat through and dig a bit deeper into the topics within your onboarding.

The goal in all of this is to ensure that you feel confident and comfortable to hit the ground running at the start of your second month on the job when you will have a [ramped quota](/handbook/marketing/sales-development/#bdr-and-sdr-compensation-and-quota). Our hope is that along this journey you are not only gaining the tactical skills needed to complete your job, but that you form connections with colleagues, gain a strong understanding of our culture, and begin to develop industry knowledge.

## Sales Development Onboarding Process

1. The People Team initiates a general GitLab onboarding issue for every new GitLab team member. On your first day, you will receive a welcome email with a link to your specific onboarding issue and steps to get started. You will also meet with your manager to discuss:
    - Access to your sales development specific issue
    - Prioritizing your onboarding issues
    - How to manage calendar invites in your inbox on Day 1
1. Within 3 days of starting at GitLab, you will receive an email giving you access to [Command of the Message (CoM)](/handbook/sales/command-of-the-message/) e-learning materials. CoM is our value-driven conversation framework that will be covered during SQS.
1. During your second week, the Sales Enablement Team will send you a calendar invite for the upcoming SQS as well as an email prompting you to log in to Google Classroom to begin working through the [Sales Quick Start learning path](/handbook/sales/onboarding/sales-learning-path/). Connect with your manager to arrange travel for SQS or if you are unable to attend the next session.
1. Within a week of attending SQS, you will receive access to the 13-week Command of the Message Fast Start program.
1. In your first few weeks you will receive access to your Sales Development Technical Development training. This is to be completed within your first 180 days and ties directly to SDR [levels](/job-families/marketing/sales-development-representative/#levels).

## Graduating from Sales Development Onboarding

- Complete GitLab general onboarding issue
- Complete BDR/SDR specific onboarding issue
- Complete Google Classroom SQS prep work
- Attend the SQS workshop
- Complete the 13-week Command of the Message Fast Start program

## BDR/SDR Quota for Months 0-4 and Compensation

For BDRs and SDRs joining on the first Monday of the month

- Month 1 = 25% quota
- Month 2 = 50% quota
- Month 3 = 75% quota
- Month 4 = 100% quota

For BDRs and SDRs joining GitLab on the 15th or after of the month

- Month 0 = no quota
- Month 1 = 25% quota
- Month 2 = 50% quota
- Month 3 = 75% quota
- Month 4 = 100% quota

Quota and compensation will be assigned by the BDR/SDR Manager and discussed in the first 1:1. If a fully ramped BDR/SDR transfers BDR/SDR teams they will have a 50% ramp for month 1 and 100% quota for month 2.

## Sales Development Onboarding Resources

- [Adding yourself to the team page](/handbook/editing-handbook/#add-yourself-to-the-team-page)
- [Making changes/edits to the handbook](/handbook/editing-handbook/#editing-the-handbook)
- Questions about a handbook edit? Use the slack channels #handbook or #mr-buddies
- Questions about onboarding? Drop them in the #new_team_members slack channel

## Manager Responsibilities

- [SDR New Hire 1:1 Template](https://docs.google.com/document/d/1hiIksiDQjZBYCevA36J5Tfa4_bItuDuDOisfIK93Ihw/edit?usp=sharing)
- [BDR New Hire 1:1 Template](https://docs.google.com/document/d/1ymdIfeGhFzFLJkz0_1qZhzVlLMaTpNaiTowfdbVJEao/edit?usp=sharing)
