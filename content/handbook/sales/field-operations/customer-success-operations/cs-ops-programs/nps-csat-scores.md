---

title: "Customer CSAT surveys"
description: "A deep dive into the CSAT and NPS, how they are defined and calculated."
---



Customer Satisfaction score, or CSAT, are surveys sent directly to primary users on paid Premium and Ultimate accounts to measure satisfaction of a service, product, interaction or experience with GitLab. Each account will receive a CSAT survey no more than twice in a year. We hope to recieve from as many accounts at GitLab as possible, rather than a representative sample of accounts. This enables us to inform all account teams of their portfolio's customer satisfaction.

In order to target leaders and decision-makers on accounts, three types of users will receive an invitation to a survey:

- [GitLab admin contacts](/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/#gitlab-admin-contacts)
- Sold-To Billing Contact (if no GitLab Admin exists)
- [GitLab.com Owner user](https://docs.gitlab.com/ee/user/permissions.html)

#### Who is not contacted on a CSAT survey?

- Identified Billing or Procurement personas configured as Owners or GitLab Admins on accounts
- Guest, Reporter, Developer or Maintainer roles on accounts
- Users that have been contacted by others CSAT Surveys within the past eight weeks, such as the [Product CSAT](https://handbook.gitlab.com/handbook/product/ux/performance-indicators/csat/) survey administered by the UX Research Team.

## All-Customer CSAT Survey

This survey is performed in two parts; an email is sent to an identified contact that meets all criteria. If, after 10 days, the customer has not responded to the survey, they will receive a reminder email. The survey will stay active for 30 days to allow for any account teams to remind customers to complete it.

### Questions

1. **How satisfied are you with GitLab?**
2. **How satisfied are you with your teams’ adoption of GitLab?**
3. **How satisfied are you with your experience working with the GitLab team?**
4. **How satisfied are you with the business value provided by GitLab?**
5. **What could we be doing better?**
   - Note: Open text field
6. **Select a role that you most closely identify with:**
   - Developer or Engineer
   - Administrator
   - Team Lead or Manager
   - Security
   - Product Manager
   - Billing or Procurement

### FY25 Schedule

- **All-Customer CSAT Survey 1**
  - May 22, 2024
  - Follow-up email: May 30, 2024
  - Purpose: Create a macro-level baseline for Customer Satisfaction Scoring, not drive customer-level interactions and escalation.
- **All-Customer Survey 2**
  - January 8, 2025 (tentative)
  - Purpose: Create account-level action orientation for Customer Success and Sales organizations.

### How to View CSAT Survey Results

#### Tableau Dashboards

The CSSO team is working to make survey results available within Snowflake so that we can provide Dashboards created within Tableau for wider analysis. This will be completed by the end of Q2 FY25.

#### If you have a Gainsight license

- Gainsight: NPS and CSAT scores are written to a scorecard in the Customer 360.
  - For an overview of how CSMs use Customer360 (or C360), please refer to the [Gainsight CSM C360 Overview Page](/handbook/customer-success/csm/gainsight/c360-overview)
- SalesForce: Survey responses can also be found in the embedded Gainsight widget on any account page in Salesforce.

#### What is CSAT?

- Customer Satisfaction is a qualitative metric to ascertain a deeper understanding of the customer sentiment at a point in the life cycle.
- It is a transactional attribute to understand how a specific action, product, or experience in the customer lifecycle has gone.
- Any type of question or questions can be a CSAT survey. There are no standards for questions or metrics. It is entirely based on what GitLab is trying to learn about the customer from a question.

##### Scoring Methodology

Each question is scored by dividing the total number of customers who select “Very Satisfied” (5) or “Satisfied” (4) by the total number of responses, then multiplied by 100.

## NPS, Deprecated Surveys, and other CSAT

### What is NPS?

- Net Promoter Score is a [standardized](https://en.wikipedia.org/wiki/Net_promoter_score) numerical value from 1 to 10.
- It is NOT a determination of customer health.
- It is NOT a method of pinpointing specific product issues.

### NPS Surveys at GitLab

Customers received NPS surveys at multiple points in their lifecycle from 2021 to 2024. These are stopped as of May 1, 2024. Their results and data can be accessed [by request](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/new) to the CSSO team.

You may request data for the following NPS surveys:

- **Post-Onboarding NPS/CSAT**  
- **Pre-Renewal NPS**
- **Post-Renewal NPS/CSAT**

### Other CSAT

- Post-Scale Engagement CSAT Survey
  - Transactional to Scale/CSE customers only
- [Case Closure Survey](https://handbook.gitlab.com/handbook/sales/commercial/global_digital_smb/#email-to-case) CSAT Survey
