| Date       | Host                 | Speaker 1              | Speaker 2              | Speaker 3              |
|------------|----------------------|------------------------|------------------------|------------------------|
| 2024-06-12 | Emily Sybrant        | Veethika Mishra        | Sascha Eggenberger     | Katie Macoy            |
| 2024-06-26 | APAC                 | Libor Vanc             | Bonnie Tsang           |                        |
| 2024-07-03 | Taurie Davis         | Tim Noah               | Austin Regnery         | Graham Bachelder       |
| 2024-07-24 | Rayana Verissimo     | Mike Nichols           | Gina Doyle             |                        |
| 2024-08-07 | Jacki Bauer          | Michael Fangman        | Ian Gloude             |                        |
| 2024-08-21 | Justin Mandell       | Chad Lavimoniere       | Dan Mizzi-Harris       | Ilonah Pelaez          |
| 2024-09-04 | Chris Micek          | Lina Fowler            | Becka Lippert          |                        |
| 2024-09-18 | Andy Volpe           | Nick Leonard           | Camellia X Yang        |                        |
| 2024-10-02 | Rayana Verissimo     | Sunjung Park           | Annabel Gray           | Nick Brandt            |
| 2024-10-16 | Paul Wright          | Emily Bauman           | Divya Alagarsamy       |                        |
| 2024-10-30 | Taurie Davis         | Jeremy Elder           | Taylor Vanderhelm      | Katie Macoy            |
| 2024-11-06 | APAC                 | Alex Fracazo           | Bonnie Tsang           |                        |
| 2024-11-27 | Marcel van Remmerden | Tim Noah               | Veethika Mishra        | Pedro Moreira da Silva |
| 2024-12-11 | Emily Sybrant        | Julia Miocene          | Ian Gloude             | Ilonah Pelaez          |
| 2024-12-18 | Jacki Bauer          | Graham Bachelder       | Libor Vanc             | New hire (TBD)         |

Tip for Product Design Managers: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online [markdown generator](https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table).
